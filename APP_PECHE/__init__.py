# Un objet "app" pour utiliser la classe Flask
# Pour les personnes qui veulent savoir ce que signifie __name__ une démonstration se trouve ici :
# https://www.studytonight.com/python/_name_-as-main-method-in-python
# C'est une chaîne de caractère qui permet de savoir si on exécute le code comme script principal
# appelé directement avec Python et pas importé.
from flask import Flask, session
from flask_bcrypt import Bcrypt
from APP_PECHE.utils import create_hashid, create_hashuserid
from APP_PECHE.DATABASE import connect_db_context_manager
import os
# Objet qui fait "exister" notre application
app = Flask(__name__, template_folder="templates")
# Flask va pouvoir crypter les cookies
app.secret_key = '_vogonAmiral_)?^'.encode(encoding='UTF-8')

# Upload d'image
# l'image sera sauvegardé dans le dossier /static/img/up
dirname = os.path.dirname(__file__)
path_upload = os.path.join(dirname, 'static/img/up')
path_thumbnail = os.path.join(dirname, 'static/img/thumbnail')

app.config["IMAGE_UPLOADS"] = path_upload
app.config["THUMBNAIL_UPLOADS"] = path_thumbnail
# Extension valide à l'upload
app.config["ALLOWED_IMAGE_EXTENSIONS"] = ["JPEG", "JPG", "PNG", "GIF"]
# taille limite de l'upload à 50 MO
app.config['MAX_CONTENT_LENGTH'] = 50 * 1024 * 1024

# function jinja
app.jinja_env.globals.update(create_hashid=create_hashid)
app.jinja_env.globals.update(create_hashuserid=create_hashuserid)

# Config Bcrypt
bcrypt = Bcrypt(app)

# Doit se trouver ici... soit après l'instanciation de la classe "Flask"
# Anthony L 2020.03.25 Tout commence ici par "indiquer" les routes de l'application.
from APP_PECHE import routes, auth
from APP_PECHE.PRISES import routes_gestion_prises
from APP_PECHE.ESPECES import routes_gestion_especes
from APP_PECHE.PROFIL import routes_gestion_profil
from APP_PECHE.METHODES import  routes_gestion_methodes
