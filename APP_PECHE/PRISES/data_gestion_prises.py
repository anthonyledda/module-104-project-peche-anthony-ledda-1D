# data_gestion_prises.py
# AL 2020.05.12 Permet de gérer (CRUD) les données de la table t_prises
from flask import flash, session

from APP_PECHE.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from APP_PECHE.DATABASE.erreurs import *


class GestionPrises:
    def __init__(self):
        try:
            # DEBUG bidon dans la console
            print("dans le try de gestions prises")
            # AL 2020.05.12 La connexion à la base de données est-elle active ?
            # Renvoie une erreur si la connexion est perdue.
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Il faut connecter une base de donnée", "Danger")
            # DEBUG bidon : Pour afficher un message dans la console.
            print(f"Exception grave Classe constructeur GestionPrises {erreur.args[0]}")
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")
        print("Classe constructeur GestionPrises ")

    def prises_card_afficher_data(self):
        try:
            # la commande MySql classique est "SELECT * FROM t_prises"
            strsql_prises_afficher = """SELECT id_PriseAPhoto, id_Prise, id_Photo, PrisePhoto,
                                        NomMethode, NomEspece, PoidsPrise, TaillePrise, DatePrise, HeurePrise, T7.User, 
                                        T7.Avatar
                                        FROM t_prise_a_photo AS T1
                                        INNER JOIN t_prises AS T2 ON T1.fk_Prise = T2.id_Prise
                                        INNER JOIN t_photos AS T3 ON T1.fk_Photo = T3.id_Photo
                                        INNER JOIN t_methodes AS T4 ON T2.fk_Methode = T4.id_Methode
                                        INNER JOIN t_especes AS T5 ON T2.fk_Espece = T5.id_Espece
                                        INNER JOIN t_user_e_prise AS T6 ON T2.id_Prise = T6.fk_Prise
                                        INNER JOIN t_users AS T7 ON T6.fk_User = T7.id_User
                                        ORDER BY id_PriseAPhoto DESC"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_prises_afficher)
                # Récupère les données de la requête.
                data_prises = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_prises ", data_prises, " Type : ", type(data_prises))
                # Retourne les données du "SELECT"
                return data_prises

        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # AL 2020.05.12 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def prises_card_afficher_data_ASC(self):
        try:
            # la commande MySql classique est "SELECT * FROM t_prises"
            strsql_prises_afficher_ASC = """SELECT id_PriseAPhoto, id_Prise, id_Photo, PrisePhoto,
                                        NomMethode, NomEspece, PoidsPrise, TaillePrise, DatePrise, HeurePrise, T7.User, 
                                        T7.Avatar
                                        FROM t_prise_a_photo AS T1
                                        INNER JOIN t_prises AS T2 ON T1.fk_Prise = T2.id_Prise
                                        INNER JOIN t_photos AS T3 ON T1.fk_Photo = T3.id_Photo
                                        INNER JOIN t_methodes AS T4 ON T2.fk_Methode = T4.id_Methode
                                        INNER JOIN t_especes AS T5 ON T2.fk_Espece = T5.id_Espece
                                        INNER JOIN t_user_e_prise AS T6 ON T2.id_Prise = T6.fk_Prise
                                        INNER JOIN t_users AS T7 ON T6.fk_User = T7.id_User
                                        ORDER BY id_PriseAPhoto ASC"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_prises_afficher_ASC)
                # Récupère les données de la requête.
                data_prises_ASC = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_prises_ASC ", data_prises_ASC, " Type : ", type(data_prises_ASC))
                # Retourne les données du "SELECT"
                return data_prises_ASC

        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # AL 2020.05.12 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def prises_card_afficher_personal_data(self):
        try:
            # la commande MySql classique est "SELECT * FROM t_prises"

            strsql_prises_afficher = """SELECT id_PriseAPhoto, id_Prise, id_Photo, PrisePhoto,
                                        NomMethode, NomEspece, PoidsPrise, TaillePrise, DatePrise, HeurePrise
                                        FROM t_prise_a_photo AS T1
                                        INNER JOIN t_prises AS T2 ON T1.fk_Prise = T2.id_Prise
                                        INNER JOIN t_photos AS T3 ON T1.fk_Photo = T3.id_Photo
                                        INNER JOIN t_methodes AS T4 ON T2.fk_Methode = T4.id_Methode
                                        INNER JOIN t_especes AS T5 ON T2.fk_Espece = T5.id_Espece
                                        WHERE id_Prise IN (SELECT id_Prise FROM t_user_e_prise AS T1
                                        INNER JOIN t_prises AS T2 ON T1.fk_Prise = T2.id_Prise
                                        INNER JOIN t_users AS T3 ON T1.fk_User = T3.id_User
                                        WHERE id_User = %(value_id_current_profile)s)
                                        ORDER BY id_PriseAPhoto DESC"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_user:
                # Constitution d'un dictionnaire pour associer l'id du user sélectionné avec un nom de variable
                valeur_id_current_profil_dictionnaire = {"value_id_current_profile": session['id_account']}
                # Envoi de la commande MySql
                mc_user.execute(strsql_prises_afficher, valeur_id_current_profil_dictionnaire)
                # Récupère les données de la requête.
                data_prises = mc_user.fetchall()
                print(data_prises)
                return data_prises

        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # AL 2020.05.12 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def prises_afficher_data(self, id_prise_sel):
        print("id_prise_sel  ", id_prise_sel)
        try:
            # la commande MySql classique est "SELECT * FROM t_prises"

            strsql_prises_afficher = """SELECT id_PriseAPhoto, id_Prise, id_Photo, PrisePhoto,
                                        NomMethode, NomEspece, PoidsPrise, TaillePrise, DatePrise, HeurePrise
                                        FROM t_prise_a_photo AS T1
                                        INNER JOIN t_prises AS T2 ON T1.fk_Prise = T2.id_Prise
                                        INNER JOIN t_photos AS T3 ON T1.fk_Photo = T3.id_Photo
                                        INNER JOIN t_methodes AS T4 ON T2.fk_Methode = T4.id_Methode
                                        INNER JOIN t_especes AS T5 ON T2.fk_Espece = T5.id_Espece"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                valeur_id_prise_selected_dictionnaire = {"value_id_prise_selected": id_prise_sel}
                # Ajoute le morceau de code sql suivant
                strsql_prises_afficher += """ HAVING id_Prise= %(value_id_prise_selected)s"""
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_prises_afficher, valeur_id_prise_selected_dictionnaire)

                # Récupère les données de la requête.
                data_prises = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_prises ", data_prises, " Type : ", type(data_prises))
                # Retourne les données du "SELECT"

                return data_prises

        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # AL 2020.05.12 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def prises_afficher_info_data(self, id_prise_sel):
        try:
            strsql_prises_afficher_info = """SELECT id_PriseAInfo, id_Prise, id_Info, ConditionMeteo, TemperatureMeteo,
                                                        VentMeteo, GpsLatLocalisation, GpsLonLocalisation, PressionMeteo, HumiditeMeteo
                                                        FROM t_prise_a_info AS T1
                                                        INNER JOIN t_prises AS T2 ON T1.fk_Prise = T2.id_Prise
                                                        INNER JOIN t_infos AS T3 ON T1.fk_Info = T3.id_Info"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                valeur_id_prise_selected_dictionnaire = {"value_id_prise_selected": id_prise_sel}
                # Ajoute le morceau de code sql suivant
                strsql_prises_afficher_info += """ HAVING id_Prise= %(value_id_prise_selected)s"""
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_prises_afficher_info, valeur_id_prise_selected_dictionnaire)

                # Récupère les données de la requête.
                data_prises_info = mc_afficher.fetchone()
                # Affichage dans la console
                print("data_prises_info ", data_prises_info, " Type : ", type(data_prises_info))
                # Retourne les données du "SELECT"

                return data_prises_info

        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # AL 2020.05.12 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def add_prise_data(self, valeurs_insertion_dictionnaire):
        # AL 2020.05.12 Insertion d'une prise data
        # SQL insertion prise
        strsql_insert_prise = """INSERT INTO t_prises (id_Prise, fk_Methode, fk_Espece, PoidsPrise, TaillePrise, 
                                DatePrise, HeurePrise) VALUES (NULL, %(value_NomMethode)s,%(value_NomEspece)s,
                                %(value_PoidsPrise)s,%(value_TaillePrise)s,%(value_DatePrise)s,%(value_HeurePrise)s);"""
        # SQL variable id_prise
        strsql_set_id_prise = """SET @last_id_prise = LAST_INSERT_ID();"""
        # SQL insertion liaison prise user
        strsql_insert_prise_users = """INSERT INTO t_user_e_prise (id_UserEPrise, fk_User, fk_Prise, DateUserPrise) 
                                        VALUES (NULL, %(value_id_User)s, @last_id_prise, NULL);"""
        # SQL insertion photo
        strsql_insert_photo = """INSERT INTO t_photos (id_Photo, PrisePhoto) VALUES
                                 (NULL,%(value_PrisePhoto)s);"""
        # SQL variable id_photo
        strsql_set_id_photo = """SET @last_id_photo = LAST_INSERT_ID();"""
        # SQL insertion liaison prise photo
        strsql_insert_prise_photo = """INSERT INTO t_prise_a_photo (id_PriseAPhoto, fk_Prise, fk_Photo, DatePhoto) 
                                        VALUES (NULL, @last_id_prise, @last_id_photo, NULL);"""
        # Verification de l'existance des infos
        if valeurs_insertion_dictionnaire['value_ConditionMeteo'] == '':
            # SQL insertion info sans meteo
            strsql_insert_info = """INSERT INTO t_infos (id_Info, GpsLatLocalisation, GpsLonLocalisation) 
                                    VALUES (NULL,%(value_GpsLatLocalisation)s,%(value_GpsLonLocalisation)s);"""
        else:
            # SQL insertion info avec meteo
            strsql_insert_info = """INSERT INTO t_infos (id_Info, ConditionMeteo, TemperatureMeteo, VentMeteo, 
                                    GpsLatLocalisation, GpsLonLocalisation, PressionMeteo, HumiditeMeteo) 
                                    VALUES (NULL,%(value_ConditionMeteo)s,%(value_TemperatureMeteo)s,
                                    %(value_VentMeteo)s,%(value_GpsLatLocalisation)s,%(value_GpsLonLocalisation)s,
                                    %(value_PressionMeteo)s,%(value_HumiditeMeteo)s);"""
        # SQL variable id_info
        strsql_insert_id_info = """SET @last_id_info = LAST_INSERT_ID();"""
        # SQL insertion liaison prise info
        strsql_insert_prise_info = """INSERT INTO t_prise_a_info (id_PriseAInfo, fk_Prise, fk_Info, DateInfo) VALUES
                                                     (NULL, @last_id_prise, @last_id_info, NULL);"""

        valeur_id_current_user_dictionnaire = {"value_id_User": session['id_account']}
        # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
        # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
        # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
        # sera interprété, ainsi on fera automatiquement un commit
        with MaBaseDeDonnee() as mconn_bd:
            # Insertion de la prise
            mconn_bd.mabd_execute(strsql_insert_prise, valeurs_insertion_dictionnaire)
            # Récupération de l'id de la prise
            mconn_bd.mabd_execute(strsql_set_id_prise)
            # Insertion de la liaison entre la prise et le user
            mconn_bd.mabd_execute(strsql_insert_prise_users, valeur_id_current_user_dictionnaire)
            # Insertion de la photo
            mconn_bd.mabd_execute(strsql_insert_photo, valeurs_insertion_dictionnaire)
            # Récupération de l'id de la photo
            mconn_bd.mabd_execute(strsql_set_id_photo)
            # Insertion de la liaison entre la prise et la photo
            mconn_bd.mabd_execute(strsql_insert_prise_photo)
            # Insertion des infos
            mconn_bd.mabd_execute(strsql_insert_info, valeurs_insertion_dictionnaire)
            # Récupération de l'id des infos
            mconn_bd.mabd_execute(strsql_insert_id_info)
            # Insertion de la liaison entre la prise et les infos
            mconn_bd.mabd_execute(strsql_insert_prise_info)

    def edit_prise_data(self, valeur_id_dictionnaire):
        try:
            print(valeur_id_dictionnaire)
            # AL 2020.05.12
            # Commande MySql pour afficher le prise sélectionné dans le tableau dans le formulaire HTML
            str_sql_id_Prise = """SELECT id_PriseAPhoto, id_Prise, id_Photo, PrisePhoto, fk_Methode, fk_Espece,
                                        NomMethode, NomEspece, PoidsPrise, TaillePrise, DatePrise, HeurePrise,
                                        id_PriseAInfo, id_Prise, id_Info, ConditionMeteo, TemperatureMeteo,
                                        VentMeteo, GpsLatLocalisation, GpsLonLocalisation, PressionMeteo, HumiditeMeteo
                                        FROM t_prise_a_photo AS T1
                                        INNER JOIN t_prises AS T2 ON T1.fk_Prise = T2.id_Prise
                                        INNER JOIN t_photos AS T3 ON T1.fk_Photo = T3.id_Photo
                                        INNER JOIN t_methodes AS T4 ON T2.fk_Methode = T4.id_Methode
                                        INNER JOIN t_especes AS T5 ON T2.fk_Espece = T5.id_Espece
                                        INNER JOIN t_prise_a_info AS T6 ON T1.fk_Prise = T6.fk_Prise
                                        INNER JOIN t_infos AS T7 ON T6.fk_Info = T7.id_Info
                                        WHERE id_Prise = %(value_id_Prise)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_id_Prise, valeur_id_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except Exception as erreur:
            # AL 2020.05.12 Message en cas d'échec.
            print(f"Problème edit_prise_data Data Gestions prises numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions prises numéro de l'erreur : {erreur}", "danger")
            # AL 2020.05.12 On dérive "Exception" par le "@app.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception... Problème edit_prise_data d'un prise Data Gestions prises {erreur}")

    def update_prise_data(self, valeur_update_dictionnaire):
        try:
            print(valeur_update_dictionnaire)
            # AL 2020.05.12 Commande MySql pour la MODIFICATION de la valeur insérée du form HTML "prises_edit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            str_sql_update_Prise = """UPDATE t_prises
                                    SET fk_Methode = %(value_fk_Methode)s, 
                                    fk_Espece = %(value_fk_Espece)s, PoidsPrise = %(value_PoidsPrise)s, 
                                    TaillePrise = %(value_TaillePrise)s, DatePrise = %(value_DatePrise)s, 
                                    HeurePrise = %(value_HeurePrise)s WHERE id_Prise = %(value_id_Prise)s;"""

            if valeur_update_dictionnaire['value_ConditionMeteo'] == 'None':
                # SQL update info sans meteo
                str_sql_update_Info = """UPDATE `t_infos`
                                        INNER JOIN t_prise_a_info ON t_prise_a_info.fk_Info = t_infos.id_Info 
                                        INNER JOIN t_prises ON t_prise_a_info.fk_Prise = t_prises.id_Prise 
                                        SET GpsLatLocalisation = %(value_GpsLatLocalisation)s,
                                        GpsLonLocalisation = %(value_GpsLonLocalisation)s
                                        WHERE t_prises.id_Prise = %(value_id_Prise)s;"""
            else:
                # SQL update info avec meteo
                str_sql_update_Info = """UPDATE `t_infos`
                                    INNER JOIN t_prise_a_info ON t_prise_a_info.fk_Info = t_infos.id_Info 
                                    INNER JOIN t_prises ON t_prise_a_info.fk_Prise = t_prises.id_Prise 
                                    SET GpsLatLocalisation = %(value_GpsLatLocalisation)s,
                                    GpsLonLocalisation = %(value_GpsLonLocalisation)s,
                                    TemperatureMeteo = %(value_TemperatureMeteo)s,
                                    PressionMeteo = %(value_PressionMeteo)s,ConditionMeteo = %(value_ConditionMeteo)s,
                                    HumiditeMeteo = %(value_HumiditeMeteo)s,VentMeteo = %(value_VentMeteo)s
                                    WHERE t_prises.id_Prise = %(value_id_Prise)s;"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_update_Prise, valeur_update_dictionnaire)
                    mc_cur.execute(str_sql_update_Info, valeur_update_dictionnaire)

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # AL 2020.05.12 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème update_prise_data Data Gestions prises numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions prises numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1062:
                flash(f"Flash. Cette valeur existe déjà : {erreur}", "danger")
                # Deux façons de communiquer une erreur causée par l'insertion d'une valeur à double.
                flash('Doublon !!! Introduire une valeur différente')
                # Message en cas d'échec du bon déroulement des commandes ci-dessus.
                print(f"Problème update_prise_data Data Gestions prises numéro de l'erreur : {erreur}")

                raise Exception("Raise exception... Problème update_prise_data d'un prise DataGestionsprises {erreur}")

    def delete_select_prise_data(self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # AL 2020.05.12 Commande MySql pour la MODIFICATION de les valeures insérées du form HTML "prisesEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # Commande MySql pour afficher le prise sélectionné dans le tableau dans le formulaire HTML
            str_sql_select_id_Prise = """SELECT id_PriseAPhoto, id_Prise, id_Photo, PrisePhoto,
                                        NomMethode, NomEspece, PoidsPrise, TaillePrise, DatePrise, HeurePrise
                                        FROM t_prise_a_photo AS T1
                                        INNER JOIN t_prises AS T2 ON T1.fk_Prise = T2.id_Prise
                                        INNER JOIN t_photos AS T3 ON T1.fk_Photo = T3.id_Photo
                                        INNER JOIN t_methodes AS T4 ON T2.fk_Methode = T4.id_Methode
                                        INNER JOIN t_especes AS T5 ON T2.fk_Espece = T5.id_Espece
                                        WHERE id_Prise = %(value_id_Prise)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une gméthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_select_id_Prise, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bidon : Pour afficher un message dans la console.
            print(f"Problème delete_select_prise_data Gestions prises numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème delete_select_prise_data numéro de l'erreur : {erreur}", "danger")
            raise Exception(
                "Raise exception... Problème delete_select_prise_data d\'un prise Data Gestions prises {erreur}")

    def delete_prise_data(self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # AL 2020.05.12 Commande MySql pour EFFACER les valeures sélectionnées
            # le "%s" permet d'éviter des injections SQL "simples"
            str_sql_delete_id_Prise = """DELETE t_prise_a_photo 
                                         FROM t_prise_a_photo
                                         INNER JOIN t_prises 
                                         ON t_prise_a_photo.fk_Prise = t_prises.id_Prise
                                         WHERE id_Prise = %(value_id_Prise)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_id_Prise, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Pour afficher un message dans la console.
            print(f"Problème delete_prise_data Data Gestions prises numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions prises numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # AL 2020.05.12 Traitement spécifique de l'erreur 1451 Cannot delete or update a parent row: a foreign key constraint fails
                # en MySql le moteur INNODB empêche d'effacer un prise qui est associé à une prise dans la table intermédiaire
                # il y a une contrainte sur les FK de la table intermédiaire
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                # flash(f"Flash. IMPOSSIBLE d'effacer !!! Ce prise est associé à des prises dans la t_prises_films !!! : {erreur}", "danger")
                # Pour afficher un message dans la console.
                print(
                    f"IMPOSSIBLE d'effacer !!! Ce prise est associé à des prisees dans la t_pers_e_prise !!! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")
