import re
from _md5 import md5
from flask import render_template, request, redirect, url_for, session, flash
from APP_PECHE import app, bcrypt
from APP_PECHE.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from APP_PECHE.DATABASE.erreurs import *


# http://http://127.0.0.1:1234/users/register
@app.route('/users/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form and 'email' in request.form:
        # Créer les variables
        username = request.form['username']
        email = request.form['email']
        password = bcrypt.generate_password_hash(request.form['password']).decode('utf-8')
        avatar = 'https://www.gravatar.com/avatar/' + md5(email.encode(encoding='UTF-8', errors='strict')).hexdigest()
        print(avatar)

        # Verifier si le compte existe
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_account:
            # Envoi de la commande MySql
            mc_account.execute('SELECT * FROM t_users WHERE User = %s', username)
            # Récupère un résultat de la requête.
            account = mc_account.fetchone()

            # Si le compte existe afficher un message d'avertisement
            if account:
                flash(f"Ce compte existe déjà")
            elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
                flash(f"L'adresse email est invalide")
            elif not re.match(r'[A-Za-z0-9]+', username):
                flash(f"Le nom d'utilisateur ne doit contenir que des lettres et des chiffres")
            elif not username or not password or not email:
                flash(f"Veuillez remplir le formulaire!")
            else:
                # Le compte n'existe pas et les données du formulaire sont valide
                # L'insertion des données
                strsql_insert_account = """INSERT INTO t_users (id_User,User,Password,Mail,Avatar) VALUES 
                                        (NULL,%(value_username)s,%(value_password)s,%(value_email)s,%(value_avatar)s)"""

                valeurs_insertion_dictionnaire = {"value_username": username,
                                                  "value_password": password,
                                                  "value_email": email,
                                                  "value_avatar": avatar}

                with MaBaseDeDonnee() as mc_account:
                    # Envoi de la commande MySql
                    mc_account.mabd_execute(strsql_insert_account, valeurs_insertion_dictionnaire)
                    flash(f"Vous êtes correctement inscrit!")
                    return redirect(url_for('login'))

    elif request.method == 'POST':
        # Le formulaire est vide
        flash(f"Veuillez remplir le formulaire!")
    # Affiche le formulaire
    return render_template("authentication/register.html")


# http://http://127.0.0.1:1234/users/login
@app.route('/users/login', methods=['GET', 'POST'])
def login():
    try:
        if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
            # Créer les variables
            username = request.form['username']
            email = request.form['username']
            password = request.form['password']

            # Anthony L Vérifier l'existance du compte
            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_account:
                # Envoi de la commande MySql
                mc_account.execute('SELECT * FROM t_users WHERE User = %s OR Mail = %s', (username, email))
                # Récupère un résultat de la requête.
                account = mc_account.fetchone()

            if bcrypt.check_password_hash(account['Password'], password):
                # Créer une donnée de session, qu'on peut accéder depuis d'autres routes
                session['loggedin'] = True
                session['id_account'] = account['id_User']
                session['username'] = account['User']
                session['avatar'] = account['Avatar']
                session['rank'] = account['Rank']
                session.permanent = True
                # Redirige sur la page principale
                return redirect(url_for('home'))
            else:
                # Le compte n'existe pas ou le mpd ou nom du user est incorrect
                flash(f"Le mot de passe ou le nom d'utilisateur est incorrect")

    except pymysql.Error as erreur:
        print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
        raise MaBdErreurPyMySl(
            f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
    except Exception as erreur:
        print(f"DGG gad Exception {erreur.args}")
        raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
    except pymysql.err.IntegrityError as erreur:
        raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    return render_template("authentication/login.html")


# http://http://127.0.0.1:1234/users/logout
@app.route('/users/logout')
def logout():
    # Anthony L déconnection d'un utilisateur
    # Retirer les données dans la session, cela déconnectera l'utilisateur
    session.pop('loggedin', None)
    session.pop('id_account', None)
    session.pop('username', None)
    session.pop('avatar', None)
    session.pop('rank', None)
    # Redirige sur la page de login
    return redirect(url_for('home'))
