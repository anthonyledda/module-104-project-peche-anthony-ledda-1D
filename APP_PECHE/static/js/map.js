//map.js

// TO MAKE THE MAP APPEAR YOU MUST
// ADD YOUR ACCESS TOKEN FROM
// https://account.mapbox.com
mapboxgl.accessToken = 'pk.eyJ1Ijoidm9sa2lvIiwiYSI6ImNrNzV1bDM5ajB6Y2kzbG52MGtjYTl5aW4ifQ.kOlnW2MuCgHexEL1pEV5Dg';
var coordinates = document.getElementById('coordinates');
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/satellite-v9', // stylesheet location
    center: [0, 0], // starting position [lng, lat]
    zoom: 1 // starting zoom
});
map.setMaxZoom(12); // max zoom
var marker;  // replace marker

// Initialize the geolocate control.
var geolocate = new mapboxgl.GeolocateControl({
    positionOptions: {
    enableHighAccuracy: true
},
    trackUserLocation: true

});

// Add the control to the map.
map.addControl(geolocate);
map.on('load', function() {
    geolocate.trigger();
});

// Add zoom and rotation controls to the map.
map.addControl(new mapboxgl.NavigationControl());

// For click on the map
map.on('click', function(e) {
    // Get the location that the user clicked.
    var clickedLocation = e.lngLat;
    $('input[id$=lat]').trigger('change');
     if(typeof(marker)==='undefined')
     {
        marker = new mapboxgl.Marker({
            draggable: true
        })
            .setLngLat(clickedLocation)
            .addTo(map);
     }
     else
     {
      marker.setLngLat(clickedLocation);
     }

    // Function Draggable
    function onDragEnd() {
        var lngLat = marker.getLngLat();
        document.getElementById('lat').value = lngLat.lat; //latitude
        document.getElementById('lng').value = lngLat.lng; //longitude
    }

    marker.on('dragend', onDragEnd);

    // Get Longitude Latitude from marker
    var lngLat = marker.getLngLat();
    // Set Lng Lat value
    document.getElementById('lat').value = lngLat.lat; //latitude
    document.getElementById('lng').value = lngLat.lng; //longitude
});

////
//var ApiBaseURL = 'https://api.openweathermap.org/data/2.5/onecall/';
///*
//    https://api.openweathermap.org/data/2.5/onecall/timemachine?lat=46.81&lon=6.69&dt=1591783800&appid=e21e8fcbf6ef45f55a9718ff5fd86376
//*/
//var ApiKey = 'e21e8fcbf6ef45f55a9718ff5fd86376';
//
//// -------------------------------------------
//
//function JSONP_Weather(input) {
//    var url = ApiBaseURL + 'timemachine?lat=' + input.lat + '&lon=' + input.lon + '&dt=' + input.dt + '&appid=' + ApiKey;
//
//    jsonP(url, input.callback);
//}
//
//// -------------------------------------------
//
//// Helper Method
//function jsonP(url, callback) {
//    $.ajax({
//        type: 'GET',
//        url: url,
//        async: false,
//        contentType: "application/json",
//        jsonpCallback: callback,
//        dataType: 'jsonp',
//        success: function (json) {
//            console.dir('success');
//        },
//        error: function (e) {
//            console.log(e.message);
//        }
//    });
//}