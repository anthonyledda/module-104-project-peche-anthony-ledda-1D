// weather-edit.js

Date.prototype.getUnixTime = function() { return this.getTime()/1000|0 };
if(!Date.now) Date.now = function() { return new Date(); }
Date.time = function() { return Date.now().getUnixTime(); }

document.getElementById('edit_DatePrise_html').addEventListener("change", weatherButtonListener);
document.getElementById('edit_HeurePrise_html').addEventListener("change", weatherButtonListener);

/*$('#map').click(function(){
    //weatherButtonListener()
})*/

document.getElementById("map").addEventListener("click", weatherButtonListener);


function weatherButtonListener() {
    var input = document.getElementById('edit_DatePrise_html').value;
    var dateEntered = new Date(input);
    const dateToday = Date.now();
    var diffTime = Math.abs(dateToday - dateEntered);
    var diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    if (diffDays <= 6 && document.getElementById('edit_HeurePrise_html').value != "" && document.getElementById('lat').value != "") {
        document.getElementById('weatherButton').style.display = "block";
    }
}

function weatherBalloon() {
    var key = 'e21e8fcbf6ef45f55a9718ff5fd86376';
    var lat = document.getElementById('lat').value;
    var lon = document.getElementById('lng').value;
    var datePrise = document.getElementById('edit_DatePrise_html').value;
    var timePrise = document.getElementById('edit_HeurePrise_html').value;
    var parsedUnixTime = new Date(datePrise + ' ' + timePrise).getUnixTime();
    var dt = parsedUnixTime;
    fetch('https://api.openweathermap.org/data/2.5/onecall/timemachine?lat=' + lat + '&lon=' + lon + '&dt=' + dt + '&lang=fr&appid=' + key)
    .then(function(resp) { return resp.json() }) // Convert data to json
    .then(function(data) {

            drawWeather(data); // Call drawWeather

    })
    .catch(function() {
        // catch any errors
    });
}

function drawWeather( d ) {
        var celcius = Math.round(parseFloat(d.current.temp)-273.15);
        document.getElementById('timezone').value = d.timezone;
        document.getElementById('temp').value = celcius;
        document.getElementById('humidity').value = d.current.humidity;
        document.getElementById('vent').value = d.current.wind_speed;
        document.getElementById('conditionMeteo').value = d.current.weather[0].description;
        document.getElementById('pression').value = d.current.pressure;
        document.getElementById('weatherTable').style.display = "block";
}

document.getElementById('weatherButton').onclick = function() {
  weatherBalloon();
}
