//map-display.js

mapboxgl.accessToken = 'pk.eyJ1Ijoidm9sa2lvIiwiYSI6ImNrNzV1bDM5ajB6Y2kzbG52MGtjYTl5aW4ifQ.kOlnW2MuCgHexEL1pEV5Dg';
var lng = document.getElementById('lng').textContent
var lat = document.getElementById('lat').textContent
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/satellite-v9', // stylesheet location
    center: [lng, lat], // starting position [lng, lat]
    zoom: 10, // starting zoom
    interactive: false
});

var marker = new mapboxgl.Marker()
    .setLngLat([lng, lat])
    .addTo(map);