# routes.py
# Anthony L 2020.04.02 Pour faire des tests divers et variés.
import os

from flask import render_template, session

from APP_PECHE import app, bcrypt


@app.route('/index')
def index():
    return 'hello'

# AL 2020.05.15 Pour faire un système de login
# Code repris de la documentation FLASK
# https://pythonspot.com/login-authentication-with-flask/


@app.route('/')
def home():
    # Verifier si l'utilisateur est loggué
    if 'loggedin' in session:
        # L'utilisateur est logué accès à la page d'acceuil
        return render_template('home.html')
    # L'utilisateur n'est pas logué redirige la home
    return render_template('home.html')


