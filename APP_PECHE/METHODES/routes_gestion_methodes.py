# routes_gestion_methodes.py
# AL 2020.05.06 methodes des "routes" FLASK pour les methodes.
from _md5 import md5

from flask import render_template, flash, redirect, url_for, request, session
from APP_PECHE import app
from APP_PECHE.METHODES.data_gestion_methodes import GestionMethodes
from APP_PECHE.DATABASE.erreurs import *
import os
from werkzeug.utils import secure_filename
# AL 2020.05.10 Pour utiliser les expressions régulières REGEX
import re

# ---------------------------------------------------------------------------------------------------
# AL 2020.05.07 Définition d'une "route" /methodes/afficher
# cela va permettre de programmer les actions avant d'interagir
# avec le navigateur par la méthode "render_template"
# Pour tester http://127.0.0.1:1234/methodes/afficher
# ---------------------------------------------------------------------------------------------------
from APP_PECHE.PRISES.routes_gestion_prises import allowed_image


@app.route("/methodes/afficher", methods=['GET', 'POST'])
def methodes_afficher():
    # AL 2020.05.12 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # AL 2020.05.12 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_methodes = GestionMethodes()
            # Récupére les données grâce à une requête MySql définie dans la classe Gestionmethodes()
            # Fichier data_gestion_methodes.py
            data_methodes = obj_actions_methodes.methodes_afficher_data()
            # Pour afficher un message dans la console.
            print(" data methodes", data_methodes, "type ", type(data_methodes))

            # AL 2020.05.12 La ligns ci-après permet de donner un sentiment rassurant aux utilisateurs.
            flash("Données methodes affichées !!", "Success")
        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # AL 2020.05.12 On dérive "Exception" par le "@app.errorhandler(404)" fichier
            # "run_mon_app.py" Ainsi on peut avoir un message d'erreur personnalisé. flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # AL 2020.05.12 Envoie la page "HTML" au serveur.
    return render_template("methodes/methodes_afficher.html", data=data_methodes)


@app.route("/methodes/afficher/admin", methods=['GET', 'POST'])
def methodes_rank_afficher():
    # AL 2020.05.12 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if 'loggedin' in session:

        if request.method == "GET":
            try:
                # AL 2020.05.12 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_methodes = GestionMethodes()
                # Récupére les données grâce à une requête MySql définie dans la classe Gestionmethodes()
                # Fichier data_gestion_methodes.py
                data_methodes = obj_actions_methodes.methodes_afficher_byid_data()
                # Pour afficher un message dans la console.
                print(" data methodes", data_methodes, "type ", type(data_methodes))

                # AL 2020.05.12 La ligns ci-après permet de donner un sentiment rassurant aux utilisateurs.
                flash("Données methodes affichées !!", "Success")
            except Exception as erreur:
                print(f"RGG Erreur générale.")
                # AL 2020.05.12 On dérive "Exception" par le "@app.errorhandler(404)" fichier
                # "run_mon_app.py" Ainsi on peut avoir un message d'erreur personnalisé. flash(f"RGG Exception {erreur}")
                raise Exception(f"RGG Erreur générale. {erreur}")
                # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

        # AL 2020.05.12 Envoie la page "HTML" au serveur.
        return render_template("methodes/methodes_rank_afficher.html", data=data_methodes)
    return redirect(url_for('login'))


# ---------------------------------------------------------------------------------------------------
# Anthony L 2020.04.07 Définition d'une "route" /methodes/add ,
# cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template"
# En cas d'erreur on affiche à nouveau la page "profil_add.html"
# Pour la tester http://127.0.0.1:1234/methodes/add
# ---------------------------------------------------------------------------------------------------
@app.route("/methodes/add", methods=['GET', 'POST'])
def methodes_add():
    # Anthony L 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:
            # Anthony L 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_methodes = GestionMethodes()
            # Anthony L 2020.04.09 Récupère le contenu du champ dans le formulaire HTML "methode_add.html"
            NomMethode = request.values['NomMethode_html']

            if re.match("^$", NomMethode):
                # Anthony L 2019.03.28 Message humiliant à l'attention de l'utilisateur.
                flash(f"Le nom est...incorrecte !!", "Danger")
                # On doit afficher à nouveau le formulaire "profil_add.html" à cause des erreurs de "claviotage"
                return render_template("methodes/methodes_add.html")

            else:
                # Définition du dictionnaire
                valeurs_insertion_dictionnaire = {"value_NomMethode": NomMethode}

                obj_actions_methodes.add_methode_data(valeurs_insertion_dictionnaire)

                # Anthony L 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux
                # utilisateurs.
                flash(f"Données insérées !!", "Sucess")
                print(f"Données insérées !!")
                # On va interpréter la "route" 'methodes_afficher', car l'utilisateur
                # doit voir le nouveau methode qu'il vient d'insérer.
                return redirect(url_for('methodes_rank_afficher'))

        # Anthony L 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # Anthony L 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # Anthony L 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}")
            raise MonErreur(f"Autre erreur")

        # Anthony L 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except Exception as erreur:
            # Anthony L 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(
                f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # Anthony L 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("methodes/methodes_add.html")


# ---------------------------------------------------------------------------------------------------
# Anthony L 2020.04.07 Définition d'une "route" /methodes_edit , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un methode de methodes par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/methodes/edit', methods=['POST', 'GET'])
def methodes_edit():
    # Anthony L 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "methodes_afficher.html"
    if request.method == 'GET':
        try:
            # Récupérer la valeur de "id_Methode" du formulaire html "methodes_afficher.html"
            # l'utilisateur clique sur le lien "edit" et on récupére la valeur de "id_Methode"
            # grâce à la variable "id_Methode_edit_html"
            # <a href="{{ url_for('methodes_edit', id_Methode_edit_html=row.id_Methode) }}">Edit</a>
            id_Methode_edit = request.values['id_Methode_edit_html']

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_Methode": id_Methode_edit}

            # Anthony L 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_methodes = GestionMethodes()

            # Anthony L 2019.04.02 La commande MySql est envoyée à la BD
            data_id_Methode = obj_actions_methodes.edit_methode_data(valeur_select_dictionnaire)
            print("dataIdmethode ", data_id_Methode, "type ", type(data_id_Methode))
            # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Editer le methode d'un film !!!")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # Anthony L 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("methodes/methodes_edit.html", data=data_id_Methode)


# ---------------------------------------------------------------------------------------------------
# Anthony L 2020.04.07 Définition d'une "route" /methodes_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un methode de methodes par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/methodes/update', methods=['POST', 'GET'])
def methodes_update():
    # Anthony L 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "methodes_afficher.html"
    # Une fois que l'utilisateur à modifié la valeur du methode alors il va appuyer sur le bouton "UPDATE"
    # donc en "POST"
    if request.method == 'POST':
        try:

            # Récupérer la valeur de "id_Methode" du formulaire html "methodes_edit.html"
            # l'utilisateur clique sur le lien "edit" et on récupére la valeur de "id_Methode"
            # grâce à la variable "id_Methode_edit_html"
            # <a href="{{ url_for('methodes_edit', id_Methode_edit_html=row.id_Methode) }}">Edit</a>
            id_Methode_edit = request.values['id_Methode_edit_html']

            # Récupère le contenu du champ "NomPers" dans le formulaire HTML "methodesEdit.html"
            NomMethode = request.values['edit_NomMethode_html']
            ValideMethode = request.values['edit_ValideMethode_html']

            valeur_edit_list = [{'id_Methode': id_Methode_edit,
                                 'NomMethode': NomMethode,
                                 'ValideMethode': ValideMethode}]

            # Regex n'acceptant que les noms valides
            if re.match("^$", NomMethode):
                # En cas d'erreur, conserve la saisie fausse, afin que l'utilisateur constate sa misérable faute
                flash(f"Le nom n'est pas valide !! Pas de chiffres, de caractères spéciaux, d'espace"
                      f"Danger")

                # On doit afficher à nouveau le formulaire "methodes_edit.html" à cause des erreurs de "claviotage"
                valeur_edit_list = [{'id_Methode': id_Methode_edit,
                                     'NomMethode': NomMethode,
                                     'ValideMethode': ValideMethode}]

                # Pour afficher le contenu et le type de valeurs passées au formulaire "methodes_edit.html"
                print(valeur_edit_list, "type ..", type(valeur_edit_list))
                return render_template('methodes/methodes_edit.html', data=valeur_edit_list)

            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_id_Methode": id_Methode_edit,
                                              "value_NomMethode": NomMethode,
                                              "value_ValideMethode": ValideMethode}

                # Anthony L 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_methodes = GestionMethodes()

                # La commande MySql est envoyée à la BD
                data_id_Methode = obj_actions_methodes.update_methode_data(valeur_update_dictionnaire)
                # DEBUG bon marché :
                print("dataIdmethode ", data_id_Methode, "type ", type(data_id_Methode))
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Editer le methode d'un film !!!")
                # On affiche les methodes
                return redirect(url_for('methodes_rank_afficher'))

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            print(erreur.args)
            flash(f"problème methodes update{erreur.args[0]}")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans le champ "name_edit_intitule_methode_html" alors on renvoie le formulaire "EDIT"
            return render_template('methodes/methodes_edit.html', data=valeur_edit_list)


# ---------------------------------------------------------------------------------------------------
# Anthony L 2020.04.07 Définition d'une "route" /methodes_select_delete , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un methode de methodes par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/methodes/select_delete', methods=['POST', 'GET'])
def methodes_select_delete():
    if request.method == 'GET':
        try:

            # Anthony L 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_methodes = GestionMethodes()
            # Anthony L 2019.04.04 Récupérer la valeur de "idmethodeDeleteHTML" du formulaire html "methodesDelete.html"
            id_Methode_delete = request.args.get('id_Methode_delete_html')
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_Methode": id_Methode_delete}

            # Anthony L 2019.04.02 La commande MySql est envoyée à la BD
            data_id_Methode = obj_actions_methodes.delete_select_methode_data(valeur_delete_dictionnaire)
            flash(f"EFFACER la ligne !")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # Pour afficher un message dans la console.
            print(f"Erreur methodes_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur methodes_delete {erreur.args[0], erreur.args[1]}")

    # Envoie la page "HTML" au serveur.
    return render_template('methodes/methodes_delete.html', data=data_id_Methode)


# ---------------------------------------------------------------------------------------------------
# Anthony L 2019.04.02 Définition d'une "route" /methodesUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier un methode, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@app.route('/methodes/delete', methods=['POST', 'GET'])
def methodes_delete():
    # Anthony L 2019.04.02 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # Anthony L 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_methodes = GestionMethodes()
            # Anthony L 2019.04.02 Récupérer la valeur de "id_Methode" du formulaire html "methodesAfficher.html"
            id_Methode_delete = request.form['id_Methode_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_Methode": id_Methode_delete}

            data_methodes = obj_actions_methodes.delete_methode_data(valeur_delete_dictionnaire)
            # Anthony L 2019.04.02 On va afficher la liste des methodes des methodes
            # Anthony L 2019.04.02 Envoie la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les methodes
            return redirect(url_for('methodes_rank_afficher'))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # Anthony L 2020.04.09 Traiter spécifiquement l'erreur MySql 1451
            # Cette erreur 1451, signifie qu'on veut effacer un "methode" de methodes qui est associé dans "t_pers_a_methodes".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE d\'effacer !!! Cette valeur est associée à des prises !')
                # Pour afficher un message dans la console.
                print(
                    f"IMPOSSIBLE d'effacer !! Ce methode est associé à des prises !!! : {erreur}")
                # Afficher la liste des methodes des methodes
                return redirect(url_for('methodes_rank_afficher'))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # Pour afficher un message dans la console.
                print(f"Erreur methodes_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur methodes_delete {erreur.args[0], erreur.args[1]}")

            # Anthony L 2019.04.02 Envoie la page "HTML" au serveur.
    return render_template('methodes/methodes_rank_afficher.html', data=data_methodes)
