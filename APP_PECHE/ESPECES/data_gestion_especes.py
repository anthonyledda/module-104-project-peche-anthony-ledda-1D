# data_gestion_especes.py
# AL 2020.05.12 Permet de gérer (CRUD) les données de la table t_especes
from flask import flash

from APP_PECHE.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from APP_PECHE.DATABASE.erreurs import *


class GestionEspeces:
    def __init__(self):
        try:
            # Affichage des données dans la console
            print("dans le try de gestions especes")
            # AL 2020.05.12 La connexion à la base de données est-elle active ?
            # Renvoie une erreur si la connexion est perdue.
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Il faut connecter une base de donnée", "Danger")
            print(f"Exception grave Classe constructeur GestionEspeces {erreur.args[0]}")
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")
        print("Classe constructeur GestionEspeces ")

    def especes_afficher_data(self):
        try:
            # la commande MySql classique est "SELECT * FROM t_especes"
            strsql_especes_afficher = """SELECT id_Espece, ThumbnailEspeceLink, NomEspece, 
            NomScientifiqueEspece, DescriptionEspece, PoidsMaxEspece, LongMaxEspece, AgeMaxEspece, ProfEspece, PlanEauEspece FROM t_especes WHERE ThumbnailEspeceLink IS NOT NULL ORDER BY NomEspece ASC"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_especes_afficher)
                # Récupère les données de la requête.
                data_especes = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_especes ", data_especes, " Type : ", type(data_especes))
                # Retourne les données du "SELECT"
                return data_especes
        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # AL 2020.05.12 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def especes_afficher_byid_data(self):
        try:
            # la commande MySql classique est "SELECT * FROM t_especes"
            strsql_especes_afficher = """SELECT id_Espece, ThumbnailEspeceLink, NomEspece, 
               NomScientifiqueEspece, DescriptionEspece, PoidsMaxEspece, LongMaxEspece, AgeMaxEspece, ProfEspece, 
               PlanEauEspece FROM t_especes ORDER BY id_Espece ASC"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_especes_afficher)
                # Récupère les données de la requête.
                data_especes = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_especes ", data_especes, " Type : ", type(data_especes))
                # Retourne les données du "SELECT"
                return data_especes
        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # AL 2020.05.12 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")


    def research_espece_data(self, valeurs_insertion_dictionnaire):
        # AL 01.07.2020 code pour recherche espece
        try:
            print(valeurs_insertion_dictionnaire)

            search_espece = valeurs_insertion_dictionnaire["value_SearchEspece"]
            print("Valeur recherchée", search_espece)

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute("""SELECT * FROM t_especes
                                      WHERE NomEspece LIKE '%%%s%%' """ % (search_espece))
                    
                    data_search = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_search)
                    return data_search

        except Exception as erreur:
            # AL 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème edit_genre_data Data Gestions Especes numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions Especes numéro de l'erreur : {erreur}", "danger")
            # AL 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception... Problème edit_genre_data d'un genre Data Gestions Espece {erreur}")
        

    def add_espece_data(self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)
            # Anthony L 2020.04.07 Insertion t_especes MySql
            strsql_insert_espece = """INSERT INTO t_especes (id_Espece,NomEspece,NomScientifiqueEspece,
                                      DescriptionEspece,PoidsMaxEspece,LongMaxEspece,AgeMaxEspece,ProfEspece,
                                      PlanEauEspece,ThumbnailEspeceLink) 
                                      VALUES (NULL,%(value_NomEspece)s,%(value_NomScientifiqueEspece)s,
                                      %(value_DescriptionEspece)s,%(value_PoidsMaxEspece)s,
                                      %(value_LongMaxEspece)s,%(value_AgeMaxEspece)s,%(value_ProfEspece)s,
                                      %(value_PlanEauEspece)s,%(value_ThumbnailEspeceLink)s)"""
            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_insert_espece, valeurs_insertion_dictionnaire)


        except pymysql.err.IntegrityError as erreur:
            # Anthony L 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        
    def edit_espece_data(self, valeur_id_dictionnaire):
        try:
            print(valeur_id_dictionnaire)
            # Anthony L 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le espece sélectionné dans le tableau dans le formulaire HTML
            str_sql_id_espece = "SELECT * FROM t_especes WHERE id_Espece = %(value_id_espece)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_id_espece, valeur_id_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except Exception as erreur:
            # Anthony L 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème edit_espece_data Data Gestions especes numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions especes numéro de l'erreur : {erreur}", "danger")
            # Anthony L 2020.04.09 On dérive "Exception" par le "@app.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception... Problème edit_espece_data d'un espece Data Gestions especes {erreur}")

    def update_espece_data(self, valeur_update_dictionnaire):
        try:
            print(valeur_update_dictionnaire)
            # Anthony L 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditIntituleespeceHTML" du form HTML "especesEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditIntituleespeceHTML" value="{{ row.NomPers }}"/></td>
            str_sql_update_NomPers = """UPDATE t_especes SET NomEspece = %(value_NomEspece)s,
                                      NomScientifiqueEspece = %(value_NomScientifiqueEspece)s, 
                                      DescriptionEspece = %(value_DescriptionEspece)s,
                                      PoidsMaxEspece = %(value_PoidsMaxEspece)s,
                                      LongMaxEspece = %(value_LongMaxEspece)s,
                                      AgeMaxEspece = %(value_AgeMaxEspece)s,
                                      ProfEspece = %(value_ProfEspece)s,
                                      PlanEauEspece = %(value_PlanEauEspece)s
                                      WHERE id_Espece = %(value_id_espece)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_update_NomPers, valeur_update_dictionnaire)

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Anthony L 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème update_espece_data Data Gestions especes numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions especes numéro de l'erreur : {erreur}", "danger")
            # raise Exception('Raise exception... Problème update_espece_data d\'un espece Data Gestions especes {}'.format(str(erreur)))
            if erreur.args[0] == 1062:
                flash(f"Flash. Cette valeur existe déjà : {erreur}", "danger")
                # Deux façons de communiquer une erreur causée par l'insertion d'une valeur à double.
                flash('Doublon !!! Introduire une valeur différente')
                # Message en cas d'échec du bon déroulement des commandes ci-dessus.
                print(f"Problème update_espece_data Data Gestions especes numéro de l'erreur : {erreur}")

                raise Exception("Raise exception... Problème update_espece_data d'un espece DataGestionsespeces {erreur}")

    def delete_select_espece_data(self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # Anthony L 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditIntituleespeceHTML" du form HTML "especesEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditIntituleespeceHTML" value="{{ row.NomPers }}"/></td>

            # Anthony L 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le espece sélectionné dans le tableau dans le formulaire HTML
            str_sql_select_id_espece = """SELECT * FROM t_especes WHERE id_Espece = %(value_id_espece)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une gméthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_select_id_espece, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Pour afficher un message dans la console.
            print(f"Problème delete_select_espece_data Gestions especes numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème delete_select_espece_data numéro de l'erreur : {erreur}", "danger")
            raise Exception("Raise exception... Problème delete_select_espece_data d\'un espece Data Gestions especes {erreur}")


    def delete_espece_data(self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # Anthony L 2019.04.02 Commande MySql pour EFFACER la valeur sélectionnée par le "bouton" du form HTML "especesEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditIntituleespeceHTML" value="{{ row.NomPers }}"/></td>
            str_sql_delete_NomPers = "DELETE FROM t_especes WHERE id_Espece = %(value_id_espece)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_NomPers, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...",data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Pour afficher un message dans la console.
            print(f"Problème delete_espece_data Data Gestions especes numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions especes numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # Anthony L 2020.04.09 Traitement spécifique de l'erreur 1451 Cannot delete or update a parent row: a foreign key constraint fails
                # en MySql le moteur INNODB empêche d'effacer un espece qui est associé à une espece dans la table intermédiaire "t_especes_espece"
                # il y a une contrainte sur les FK de la table intermédiaire "t_especes_especes"
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                # flash(f"Flash. IMPOSSIBLE d'effacer !!! Ce espece est associé à des especes dans la t_especes_films !!! : {erreur}", "danger")
                # Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !!! Ce espece est associé à des especees dans la t_pers_a_especes !!! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")