# routes_gestion_profil.py
# AL 2020.05.06 profil des "routes" FLASK pour les profil.
from _md5 import md5

from flask import render_template, flash, redirect, url_for, request, session
from APP_PECHE import app
from APP_PECHE.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from APP_PECHE.PRISES.data_gestion_prises import GestionPrises
from APP_PECHE.PROFIL.data_gestion_profil import GestionProfil
from APP_PECHE.DATABASE.erreurs import *
import re
import os


# ---------------------------------------------------------------------------------------------------
# Anthony L 2020.04.07 Définition d'une "route" /profile
# cela va permettre de programmer les actions avant d'interagir
# avec le navigateur par la méthode "render_template"
# Pour tester http://127.0.0.1:1234/users/profile
# ---------------------------------------------------------------------------------------------------
@app.route("/users/profile")
def profile():
    # Vérifier si l'utilisateur est loggué
    if 'loggedin' in session:
        try:
            # Anthony L 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_profil = GestionProfil()
            # Récupére les données grâce à une requête MySql définie dans la classe Gestionprofil()
            # Fichier data_gestion_profil.py
            account = obj_actions_profil.profil_afficher_data()
            # Récupère les prises lié au profil
            obj_actions_prises = GestionPrises()
            data_prises = obj_actions_prises.prises_card_afficher_personal_data()

            return render_template('profil/profil_afficher.html',
                                   account=account,
                                   data=data_prises)
        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # Anthony L 2020.04.09 On dérive "Exception" par le "@app.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Anthony L 2020.04.07 Envoie la page "HTML" au serveur.
    return redirect(url_for('login'))


# ---------------------------------------------------------------------------------------------------
# AL 2020.05.12 Définition d'une "route" /prises_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un prise de prises par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/users/profile/update', methods=['POST', 'GET'])
def profile_update():
    # AL 2020.05.12 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "prises_cards.html"
    # Une fois que l'utilisateur à modifié la valeur du prise alors il va appuyer sur le bouton "UPDATE"
    # donc en "POST"

    if request.method == 'POST':
        try:
            # Récupère le contenu des champs dans le formulaire HTML "prises_edit.html"
            User = request.form['user_html']
            PrenomPers = request.form['prenom_html']
            NomPers = request.form['nom_html']
            Mail = request.values['email_html']
            Avatar = 'https://www.gravatar.com/avatar/' + md5(
                Mail.encode(encoding='UTF-8', errors='strict')).hexdigest()
            DateNaissPers = request.values['birthdate_html']

            valeur_edit_list = [{'User': User, 'PrenomPers': PrenomPers,
                                 'NomPers': NomPers, 'Mail': Mail,
                                 'Avatar': Avatar,
                                 'DateNaissPers': DateNaissPers}]

            # Regex email
            if not re.match(
                    "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$",
                    Mail):
                # AL 2020.05.06 Message indiquant le probleme à l'utilisateur.
                flash(f"L'adresse email est incorrecte!")
                return redirect(url_for('profile'))

            if session['username'] != User:
                # Verifier si le compte existe
                with MaBaseDeDonnee().connexion_bd.cursor() as mc_account:
                    # Envoi de la commande MySql
                    mc_account.execute('SELECT * FROM t_users WHERE User = %s', User)
                    # Récupère un résultat de la requête.
                    account = mc_account.fetchone()

                    # Si le compte existe afficher un message d'avertisement
                    if account:
                        flash(f"Ce username existe déjà")
                        return redirect(url_for('profile'))
                    else:
                        # Constitution d'un dictionnaire et insertion dans la BD
                        valeur_update_dictionnaire = {'value_User': User,
                                                      "value_PrenomPers": PrenomPers,
                                                      "value_NomPers": NomPers,
                                                      "value_Mail": Mail,
                                                      'value_Avatar': Avatar,
                                                      "value_DateNaissPers": DateNaissPers,
                                                      "value_id_current_profile": session['id_account']}

                        # AL 2020.05.12 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                        obj_actions_profil = GestionProfil()
                        # La commande MySql est envoyée à la BD
                        account = obj_actions_profil.profil_update_data(valeur_update_dictionnaire)
                        print("account ", account)

                        # On redéfinit les variables de session
                        session['username'] = User
                        session['avatar'] = Avatar

                        # On affiche le compte
                        return redirect(url_for('profile'))

            elif session['avatar'] != Avatar:
                # Verifier si le compte existe
                with MaBaseDeDonnee().connexion_bd.cursor() as mc_account:
                    # Envoi de la commande MySql
                    mc_account.execute('SELECT * FROM t_users WHERE Mail = %s', Mail)
                    # Récupère un résultat de la requête.
                    account = mc_account.fetchone()

                    # Si le compte existe afficher un message d'avertisement
                    if account:
                        flash(f"Cet email existe déjà")
                        return redirect(url_for('profile'))
                    else:
                        # Constitution d'un dictionnaire et insertion dans la BD
                        valeur_update_dictionnaire = {'value_User': User,
                                                      "value_PrenomPers": PrenomPers,
                                                      "value_NomPers": NomPers,
                                                      "value_Mail": Mail,
                                                      'value_Avatar': Avatar,
                                                      "value_DateNaissPers": DateNaissPers,
                                                      "value_id_current_profile": session['id_account']}

                        # AL 2020.05.12 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                        obj_actions_profil = GestionProfil()
                        # La commande MySql est envoyée à la BD
                        account = obj_actions_profil.profil_update_data(valeur_update_dictionnaire)
                        print("account ", account)

                        # On redéfinit les variables de session
                        session['username'] = User
                        session['avatar'] = Avatar

                        # On affiche le compte
                        return redirect(url_for('profile'))
            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {'value_User': User,
                                              "value_PrenomPers": PrenomPers,
                                              "value_NomPers": NomPers,
                                              "value_Mail": Mail,
                                              'value_Avatar': Avatar,
                                              "value_DateNaissPers": DateNaissPers,
                                              "value_id_current_profile": session['id_account']}

                # AL 2020.05.12 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_profil = GestionProfil()
                # La commande MySql est envoyée à la BD
                account = obj_actions_profil.profil_update_data(valeur_update_dictionnaire)
                print("account ", account)

                # On redéfinit les variables de session
                session['username'] = User
                session['avatar'] = Avatar

                # On affiche le compte
                return redirect(url_for('profile'))

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            print(erreur.args)
            flash(f"problème profiles update{erreur.args[0]}")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" on renvoie le formulaire "EDIT"
            return redirect(url_for('profile'))


# ---------------------------------------------------------------------------------------------------
# AL 2020.05.12 Définition d'une "route" /profile/delete , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de supprimer un compte
# ---------------------------------------------------------------------------------------------------
@app.route('/users/profile/delete', methods=['POST', 'GET'])
def profile_delete():
    # AL 2020.05.12 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # AL 2020.05.12 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_profile = GestionProfil()
            # AL 2020.05.12 Récupérer la valeur de "id_Prise"
            id_User_delete = request.form['id_User_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_User": id_User_delete}

            obj_actions_profile.profile_delete_data(valeur_delete_dictionnaire)

            # On affiche le page d'accueil
            return redirect(url_for('logout'))

        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # AL 2020.05.12 Traiter spécifiquement l'erreur MySql 1451 Cette erreur 1451, signifie qu'on veut effacer
            # un "prise" de prises qui est associé dans "t_pers_a_prises".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE d\'effacer !!! Cette valeur est associée à des prisess !')
                # Pour afficher un message dans la console.
                print(
                    f"IMPOSSIBLE d'effacer !! Ce user est associé à des prises dans la t_user_a_prises !!! : {erreur}")
                # Afficher la liste des prises des prises
                return redirect(url_for('profile'))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # Pour afficher un message dans la console.
                print(f"Erreur prises_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur prises_delete {erreur.args[0], erreur.args[1]}")
                return redirect(url_for('profile'))
    # Anthony L 2019.04.02 Envoie la page "HTML" au serveur.
    return render_template('profil/profil_afficher.html')